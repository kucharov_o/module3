package com.epam.esm.entity;

import com.sun.org.glassfish.gmbal.ManagedAttribute;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class GiftCertificate extends AbsEntity {

    private String description;

    private String name;

    private Double price;


    private Integer duration;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Tag> tags;
}
